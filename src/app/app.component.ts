import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LoginComponent } from './components/login/login.component';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent {

    constructor() {
    }

}
