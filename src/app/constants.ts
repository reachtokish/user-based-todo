export const API_BASE_URL: string = 'https://todoapplication-7d62d.firebaseio.com';
export const MONTHS: Array<string> = ["Januar", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
export let CURRENT_USER_ID: any = JSON.parse(localStorage.getItem("storedData")) == undefined ? '' : JSON.parse(localStorage.getItem("storedData")).id;