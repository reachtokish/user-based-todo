import { Component, OnInit } from '@angular/core';
import { API_BASE_URL, MONTHS } from './../../constants';
import { HttpService } from './../../services/http.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'createTodo',
    templateUrl: './createTodo.component.html',
    styleUrls: ['./../../todo.component.css']
})

export class CreateTodoComponent {

    constructor(private http: HttpService, private router: Router) { }

    public todoData = {
    	title: '',
    	status: '',
    	description: '',
    	createdOn: ''
    }

    public userData = JSON.parse(localStorage.getItem("storedData"));

    public doCreateTodo(form) {

    	let currentDate = new Date();
    	this.todoData.createdOn = currentDate.toString();

    	this.http.postData(API_BASE_URL + "/users/" + this.userData.id + "/todos.json", this.todoData)
    		.subscribe((response) => {
    			form.resetForm();
    			this.todoData = {
			    	title: '',
			    	status: '',
			    	description: '',
			    	createdOn: ''
			    }
			    this.router.navigate(['/todos']);
    		});
    }

}
