import { Component, Output, EventEmitter, OnInit, } from '@angular/core';
import { API_BASE_URL } from './../../constants';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpService } from './../../services/http.service';

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./../../auth.component.css']
})

export class LoginComponent {

    public loginChecking: boolean;
    public loginFailed: boolean;
    public loginSuccess: boolean;

    constructor(private http: HttpService, private router: Router) { }

    ngOnInit() {
        this.loginChecking = false;
        this.loginFailed = false;
        this.loginSuccess = false;
        localStorage.removeItem("storedData");
    }

    loginData = {
        email: "",
        password: ""
    }

    public doLogin(email, password, form) {
        this.loginChecking = true;
        if(email.value != ''){
            this.http.getData(API_BASE_URL + "/users.json")
                .subscribe((response) => {
                    // console.log(response);
                    if(response == null) {
                        alert("Null Response");
                    }
                    else {
                        var responseKeysArr = Object.keys(response);
                        for(let i of responseKeysArr) {
                            if(response[i].email == email.value && response[i].password == password.value){
                                // console.log("Matched");
                                this.loginChecking = false;
                                this.loginSuccess = true;
                                this.loginFailed = false;
                                let storedData = {
                                    email: response[i].email,
                                    username: response[i].userName,
                                    id: i
                                }
                                localStorage.setItem("storedData", JSON.stringify(storedData));
                                setTimeout(() => {
                                    this.router.navigate(['/todos']);
                                }, 2000);
                                return;
                            }
                            else {
                                // console.log("Not Matched");
                                this.loginChecking = false;
                                this.loginFailed = true;
                                this.loginSuccess = false;
                            }
                        }
                    }
                })
        }
    }

}
