import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'logout',
    templateUrl: './logout.component.html'
})

export class LogoutComponent {

    constructor(private router: Router) { }

    public doLogout() {
        this.router.navigate(['/login']);
        localStorage.removeItem("storedData");
    }

}
