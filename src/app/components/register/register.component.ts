import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_BASE_URL } from './../../constants';
import { HttpService } from './../../services/http.service';
import { CheckEmailExistService } from './../../services/checkEmailExist.service';
import { AbstractControl } from '@angular/forms';

@Component({
    selector: 'register',
    templateUrl: './register.component.html',
    styleUrls: ['./../../auth.component.css']
})

export class RegisterComponent {

    constructor(private http: HttpService, public checkEmailExist: CheckEmailExistService) { }

    ngOnInit() { }

    registerData = {
        userName: '',
        email: '',
        password: ''
    }

    doRegister(email, form) {
        this.checkEmailExist.checkResponse(email.value);
        if(this.checkEmailExist.emailExist != true){
            this.http.postData(API_BASE_URL + "/users.json", this.registerData)
                .subscribe(data => {
                    this.http.getData(API_BASE_URL + "/users.json")
                        .subscribe(response => {
                            form.resetForm();
                        })
                })
        }
    }

    checkEmail(email) {
        this.checkEmailExist.checkResponse(email.value);
    }

}
