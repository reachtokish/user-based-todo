import { Component, OnInit } from '@angular/core';
import { API_BASE_URL } from './../../constants';
import { CURRENT_USER_ID } from './../../constants';
import { HttpService } from './../../services/http.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'edit-todo',
    templateUrl: './editTodo.component.html',
    styleUrls: ['./../../todo.component.css']
})

export class EditTodoComponent implements OnInit {

    constructor(private http: HttpService, private router: Router, public route: ActivatedRoute) { }

    public todoData = {
        title: '',
        status: '',
        description: ''
    }

    ngOnInit() {

        this.todoData = {
            title: this.route.snapshot.paramMap.get("title"),
            status: this.route.snapshot.paramMap.get("status"),
            description: this.route.snapshot.paramMap.get("description")
        }

    }

    doEditTodo(form) {
        let current_user_id = JSON.parse(localStorage.getItem("storedData")).id;
        console.log(current_user_id);
        this.http.updateData(API_BASE_URL + "/users/" + current_user_id + "/todos/" + this.route.snapshot.paramMap.get("id") + ".json", this.todoData)
            .subscribe((response) => {
                if(response) {
                    this.router.navigate(["/todos"]);
                }
                else {
                    alert("Bad query");
                }
            });
    }

}
