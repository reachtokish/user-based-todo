import { Component, OnInit } from '@angular/core';
import { API_BASE_URL } from './../../constants';
import { CURRENT_USER_ID } from './../../constants';
import { HttpService } from './../../services/http.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'todos',
    templateUrl: './todos.component.html',
    styleUrls: ['./../../todo.component.css']
})

export class TodosComponent implements OnInit {

    constructor(private http: HttpService, private router: Router) { }

    public userData = JSON.parse(localStorage.getItem("storedData"));

    public todoList = [];

    public todoLoading: boolean;

    ngOnInit() {

    	this.loadData();

    }

    public loadData() {

    	this.todoLoading = true;
    	this.todoList = [];

    	this.http.getData(API_BASE_URL + "/users/" + this.userData.id + "/todos.json")
    		.subscribe((response) => {
    			var responseKeysArr = response == null ? [] : Object.keys(response);
    			for(var i of responseKeysArr) {
    				let eachTodoObject = response[i];
    				eachTodoObject.id = i;
    				this.todoList.push(eachTodoObject);
    			}
    			this.todoLoading = false;
    			// console.log(this.todoList);
    		});

    }

    public doDelete(todoId) {
    	console.log(API_BASE_URL + "/users/" + CURRENT_USER_ID + "/todos/" + todoId + ".json");
    	this.http.deleteData(API_BASE_URL + "/users/" + CURRENT_USER_ID + "/todos/" + todoId + ".json")
    		.subscribe((response) => {
    			console.log(response);
    			if(response == null) {
    				this.loadData();
    			}
    			else {
    				alert("Wrong query");
    			}
    		})
    }

    public doChangeStatus(status, todoId) {
    	let updateData = {
    		status: status
    	}
    	this.http.updateData(API_BASE_URL + "/users/" + CURRENT_USER_ID + "/todos/" + todoId + ".json", updateData)
    		.subscribe((response) => {
    			if(response) {
    				this.loadData();
    			}
    			else {
    				alert("Wrong query");
    			}
    		})
    }

}
