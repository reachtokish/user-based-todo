import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { TodosComponent } from './components/todos/todos.component';
import { CreateTodoComponent } from './components/create-todo/createTodo.component';
import { EditTodoComponent } from './components/edit-todo/editTodo.component';
import { AuthGuard } from './services/authGuard.service';

export const router:Routes = [
	{
		path: '',
		redirectTo: '/login',
		pathMatch: 'full'
	},
	{
		path: 'login',
		component: LoginComponent
	},
	{
		path: 'register',
		component: RegisterComponent
	},
	{
		path: 'todos',
		component: TodosComponent,
		canActivate: [AuthGuard]
	},
	{
		path: 'create-todo',
		component: CreateTodoComponent,
		canActivate: [AuthGuard]
	},
	{
		path: 'edit-todo',
		component: EditTodoComponent,
		canActivate: [AuthGuard]
	}
]

export const routes:ModuleWithProviders = RouterModule.forRoot(router);
