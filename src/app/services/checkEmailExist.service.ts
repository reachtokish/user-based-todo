import { Injectable } from '@angular/core';
import { API_BASE_URL } from './../constants';
import { HttpService } from './http.service';

@Injectable()
export class CheckEmailExistService {

    emailExist: boolean;
    checkingEmailExist: boolean;

    constructor(private http: HttpService) {
        this.emailExist = false;
        this.checkingEmailExist = false;
    }

    checkResponse(email) {
        if(email != ''){
            this.checkingEmailExist = true;
            this.http.getData(API_BASE_URL + "/users.json")
                .subscribe((response) => {
                    if(response == null) {
                        alert("Null Response");
                    }
                    else {
                        var responseKeysArr = Object.keys(response);
                        for(let i of responseKeysArr) {
                            if(response[i].email == email){
                                this.emailExist = true;
                                this.checkingEmailExist = false;
                                return;
                            }
                            else {
                                this.emailExist = false;
                                this.checkingEmailExist = false;
                                return;
                            }
                        }
                    }
                    this.checkingEmailExist = false;
                })
        }
    }

}
