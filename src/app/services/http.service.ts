import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class HttpService {

    constructor(private http:HttpClient) { }

    getData(url) {
        return this.http.get(url);
    }

    postData(url, data) {
        return this.http.post(url, data);
    }

    updateData(url, data) {
        return this.http.patch(url, data);
    }

    deleteData(url) {
        return this.http.delete(url);
    }

}
