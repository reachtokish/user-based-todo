import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { routes } from './app.router';
import { HttpService } from './services/http.service';
import { CheckEmailExistService } from './services/checkEmailExist.service';
import { AuthGuard } from './services/authGuard.service';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';

import { TodosComponent } from './components/todos/todos.component';
import { CreateTodoComponent } from './components/create-todo/createTodo.component';
import { EditTodoComponent } from './components/edit-todo/editTodo.component';

import { LogoutComponent } from './components/logout/logout.component';

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        RegisterComponent,
        TodosComponent,
        LogoutComponent,
        CreateTodoComponent,
        EditTodoComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        routes,
        FormsModule
    ],
    providers: [
        HttpService,
        CheckEmailExistService,
        AuthGuard
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule { }
